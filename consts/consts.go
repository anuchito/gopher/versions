package main

import (
	"fmt"
)

// version: v1.0.0
// commit hash: a1cfd2efaea7f6f5d07023c9e9f9f3d1e4b5c9e0
// go runtime version: go1.16.5 darwin/amd64

func main() {
	fmt.Println("version:", version)
}
