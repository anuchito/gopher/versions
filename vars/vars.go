package main

import (
	"fmt"
	"os/exec"
)

var version string

func init() {
	cmd := exec.Command("git", "rev-parse", "HEAD")
	revision, err := cmd.Output()
	if err != nil {
		panic(err)
	}
	version = fmt.Sprintf("%s:%s", "v1.0.0", revision)
}

func main() {
	fmt.Println(version)
}
