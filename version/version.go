package main

import (
	"fmt"
	"strings"
)

var version string
var revision string
var buildTime string
var tag string

type Version struct {
	Version   string `json:"version"`
	Revision  string `json:"revision"`
	BuildTime string `json:"buildTime"`
	Tag       string `json:"tag"`
}

func (v Version) String() string {
	var sb strings.Builder
	format := "%-10s : %v\n"

	sb.WriteString(fmt.Sprintf(format, "Version", v.Version))
	sb.WriteString(fmt.Sprintf(format, "Revision", v.Revision))
	sb.WriteString(fmt.Sprintf(format, "Build time", v.BuildTime))
	sb.WriteString(fmt.Sprintf(format, "Tag", v.Tag))

	return strings.TrimSuffix(sb.String(), "\n")
}

var ver Version

func init() {
	ver = Version{
		Version:   version,
		Revision:  revision,
		BuildTime: buildTime,
		Tag:       tag,
	}
}
