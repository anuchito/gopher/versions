## Release

### Steps
1. `make bump_version`
2. `make build`
	
## TODO
- [ ] Add `make bump_version` to `make build-prod`

## Tags

create: `git tag -a v0.6.0 -m "version v0.6.0 test tag"` 
delete: `git tag --delete v0.6.0`
