package main

import "testing"

func TestVersionToString(t *testing.T) {
	ver = Version{
		Version:   "v1.20.1",
		Revision:  "9d4baf0cba7955717c9e7a9b2228dd055331e0bf",
		BuildTime: "2023/05/19_Fri_10:39:37+07",
		Tag:       "v1.20.1-3-g46ddc46",
	}

	want := `Version    : v1.20.1
Revision   : 9d4baf0cba7955717c9e7a9b2228dd055331e0bf
Build time : 2023/05/19_Fri_10:39:37+07
Tag        : v1.20.1-3-g46ddc46`

	if ver.String() != want {
		t.Errorf("Version.String() = %s; want %s", ver.String(), want)
	}
}
