package config

import "os"

type DB struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
	SslMode  string `json:"sslMode"`
}
type Config struct {
	Env string `json:"env"`
	DB  DB     `json:"db"`
}

var C Config

func init() {
	C = Config{
		Env: os.Getenv("ENV"),
		DB: DB{
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			Username: os.Getenv("DB_USERNAME"),
			Password: os.Getenv("DB_PASSWORD"),
			SslMode:  os.Getenv("DB_SSL_MODE"),
		},
	}
}
